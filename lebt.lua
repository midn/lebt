local function read(fn)
	local fd = io.open(fn, "r");
	
	if not fd then return nil; end
	
	local ret = fd:read("*all");
	fd:close();
	return ret;
end

local function compile(src)
	local code = {"local RETURN_STRINGS={};"};
	
	while true do
		local startI = select(2, src:find("{[{%%]", 1, false));
		local endI;
		local isstmt = false;
		
		if startI then
			isstmt = src:byte(startI) == string.byte("%");
			endI = select(2, src:find(isstmt and "%%}" or "}}", 1, false));
		end
		
		if startI and endI then
			table.insert(code, ";table.insert(RETURN_STRINGS,"..string.format("%q", src:sub(1, startI - 2))..")");
			
			if isstmt then
				table.insert(code, src:sub(startI + 1, endI - 2));
			else
				table.insert(code, "table.insert(RETURN_STRINGS,tostring("..src:sub(startI + 1, endI - 2).."))");
			end
			
			table.insert(code, ret);

			src = src:sub(endI + 1);
		else
			table.insert(code, "table.insert(RETURN_STRINGS,"..string.format("%q", src)..")return table.concat(RETURN_STRINGS)");
			break;
		end
	end

	local ret, err = load(table.concat(code), "LEBT Render", "t");
	
	if err then error(err); end
	
	return ret;
end

local function render(tmpl, env, inherit)
	env = env or {};
	
	if inherit then setmetatable(env, {__index=inherit}); end

	local i = 1;
	while true do
		local name = debug.getupvalue(tmpl, i);
		if name == "_ENV" then
			debug.upvaluejoin(tmpl, i, (function()
				return env;
			end), 1)
			break;
		elseif not name then
			break;
		end
		i = i + 1;
	end
	
	return tmpl();
end

return {
	render = render,
	compile = compile,
	compilef = function(fn) return compile(read(fn)) end
};
