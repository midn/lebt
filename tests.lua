#!/usr/bin/env lua

local lebt = require("lebt");

function testEq(one, two)
	if one ~= two then
		error(string.format("%q ~= %q", tostring(one), tostring(two)))
	end
end

function test1()
	testEq(lebt.render(lebt.compile("Hello, {{ obj }}!"), { obj = "World" }, _G), "Hello, World!");
end

function test2()
	testEq(lebt.render(lebt.compile("{% if obj:upper():byte(2) == string.byte('O') then %}Yes{% else %}No{% end %}"), { obj = "World" }, _G), "Yes");
	testEq(lebt.render(lebt.compile("{% if obj:upper():byte(2) == string.byte('O') then %}Yes{% else %}No{% end %}"), { obj = "Heaven" }, _G), "No");
end

function test3()
	testEq(lebt.render(lebt.compile("{{ obj:upper():byte(2) == string.byte('O') }}"), { obj = "Humans" }, _G), "false");
end

function test4()
	testEq(lebt.render(lebt.compile([[{{ true
	
}} ]]), nil, _G), "true ");
end

function test5()
	testEq(lebt.render(lebt.compile([[{% local i = 1; while i <= j do %}{{i}}{% i = i + 1; end %}]]), { j = 5 }, _G), "12345");
end

-----

local tests = {test1, test2, test3, test4, test5};

local failed = 0;

for k, test in pairs(tests) do
	local result, err = pcall(test);
	
	io.write(string.format("%i... ", k));
	
	if not result then
		io.write(err);

		failed = failed + 1;
	else
		io.write("ok.");
	end
	
	io.write("\n");
end

io.write(string.format("\nFailed: %i/%i == %g%%.\n", failed, #tests, failed / #tests * 100));
