# LEBT
**L**ua **i** **f**orgot **t**he **a**cronym is a templating language inspired by Jinja2 that features Lua syntax. Minimum Lua 5.2 is required.

## Usage

```lua
local lebt = require("lebt");

local template = lebt.compile("Hello, {{obj}}!");

-- To render and print it:
print(lebt.render(template, {obj = "World"}, _G)); -- The third argument is a table to inherit, in this case _G. If you choose another one, it MUST have "table" and "tostring".
```
As you can see, `{{ e }}` is substituted with the value of the expression.

There is another tag type, `{% s %}` which runs a piece of Lua code directly, letting you use statements such as `if`.

Here's a more complicated example:
```html
<!-- In index.lhtml. -->
<!DOCTYPE html>
<html>
	<head><title>{{ title }}</title></head>
	<body>
		{% if headerisalink then %}<a href="/">{% end %}
			<h1>My Cool Site</h1>
		{% if headerisalink then %}</a>{% end %}
		
		<h2>{{ title }}</h2>
		<main>
			{{ content }}
		</main>
	</body>
</html>
```
```lua
-- In main.lua.
local lebt = require("lebt");

local template = lebt.compilef("index.lhtml");

print(lebt.render(template, {title = "Welcome", content = "make sure to like and subscribe!"}));
```

## What you might not like about LEBT

* It uses `debug.setupvalue` for performance reasons.
* It's implementation. Guess what? Everything amounts to a bunch of `table.insert` calls.
* It's probably pretty easy to break (just add a `)` at the end of a statement tag).

## License

You actually considered using this library after reading the above section? Well, then: It's dual-licensed under CC0 and the Expat License.
